﻿using System;
using System.Collections.Generic;

namespace WEBApp.DataAccess.Models
{
    public partial class Account
    {
        public int ID { get; set; }
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
