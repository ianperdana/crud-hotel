﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.Helpers
{
    public class RestAPIHelper<T>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static T Submit(string jsonBody, Method httpMethod, string endpoint)
        {
            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }

            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);
            var result = JsonConvert.DeserializeObject<T>(response.Content);
            return result;
        }

        public static T Submit(string jsonBody, Method httpMethod, string endpoint, HttpRequest httpRequest)
        {


            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            string token = null;
            var cookie = httpRequest.Cookies["token"];
            token = cookie;

            if (!string.IsNullOrEmpty(token))
            {
                requests.AddHeader("Authorization", $"Bearer {token}");
            }

            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new System.Exception("UnAuthorize");
            }

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                logger.Error(response.ErrorMessage, response.StatusCode, jsonBody, endpoint);
            }

            var result = JsonConvert.DeserializeObject<T>(response.Content);

            return result;
        }

        public static T Submit(string jsonBody, Method httpMethod, string endpoint, HttpRequest httpRequest, ref string message)
        {
            var requests = new RestRequest("/", httpMethod);

            requests.AddHeader("Content-Type", "application/json");
            requests.AddHeader("Authorization", string.Format("Bearer " + httpRequest.Cookies["token"]));
            requests.AddHeader("Connection", "keep-alive");
            requests.Timeout = (int)TimeSpan.FromHours(8).TotalMilliseconds; // 7200000 millisecond = 120 Menit

            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            var client = new RestClient(endpoint);
            //client.ReadWriteTimeout = 7200000; // 7200000 millisecond = 120 Menit
            //client.Timeout = 7200000; // 7200000 millisecond = 120 Menit
            IRestResponse response = client.Execute(requests);


            if (response.IsSuccessful)
            {
                message = response.StatusDescription;
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
            {
                return default(T);
            }
        }

        public static T SubmitExternalAPI(string jsonBody, Method httpMethod, string endpoint, string key)
        {
            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.AddHeader("Authorization", "Basic " + key);
            requests.AddHeader("X-Request-ID", Guid.NewGuid().ToString());
            requests.AddHeader("X-INDONESIAEXIMBANK-Client-Timestamp", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));
            requests.Timeout = 300000;
            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }

            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);
            var result = JsonConvert.DeserializeObject<T>(response.Content);
            return result;
        }

    }
   
}
