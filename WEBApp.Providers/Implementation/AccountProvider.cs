﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using WebApp.WEB.Models.Account;
using WEBApp.DataAccess.Contexts;
using WEBApp.DataAccess.Models;
using WEBApp.Helpers;
using WEBApp.Providers.Interface;
using WEBApp.ViewModels;
using System.Linq.Dynamic.Core;


namespace WEBApp.Providers.Implementation
{
    public class AccountProvider : IAccountProvider
    {
        private readonly HotelContext _context;
        private readonly IConfiguration _configuration;

        public AccountProvider(HotelContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public LoginResponseViewModel Login(AccountViewModel model)
        {
            LoginResponseViewModel result = new LoginResponseViewModel();
            var user = _context.Account.SingleOrDefault(x => x.Username == model.Username && x.Password == EncryptionHelper.Encrypt(model.Password,"123"));
            if (user != null)
            {
                result.Username = user.Username;
                result.Token = CreateToken(user);
            }
            return result;
        }

        public AccountViewModel Register(AccountViewModel model)
        {
            try
            {
                Account data = new Account();
                data.Username = model.Username;
                data.Password = EncryptionHelper.Encrypt(model.Password, "123");
                _context.Account.Add(data);
                _context.SaveChanges();
                return model;
            }
            catch (Exception)
            {
                throw;
            }    
            
        }
        public string CreateToken(Account account)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim("LoginID", account.ID.ToString()),
                new Claim(ClaimTypes.Name, account.Username)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AuthSettings:Key").Value));
            var token = new JwtSecurityToken(
            issuer: _configuration.GetSection("AuthSettings:Issuer").Value,
                audience: _configuration.GetSection("AuthSettings:Audience").Value,
                claims: claims,
                expires: DateTime.Now.AddMinutes(Convert.ToDouble(_configuration.GetSection("AuthSettings:TokenValidityInMinutes").Value)),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                );
            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }
        public GridViewModel<List<Account>> GetAccount(DataTablesParameters dataTableParams)
        {
            int pageSize = dataTableParams.Length != null ? Convert.ToInt32(dataTableParams.Length) : 0;
            int skip = dataTableParams.Start != null ? Convert.ToInt32(dataTableParams.Start) : 0;
            int recordsTotal = 0;
            var accountData = (from tempAccount in _context.Account select tempAccount);
            if (!(string.IsNullOrEmpty(dataTableParams.sortColumn) && string.IsNullOrEmpty(dataTableParams.sortColumnDirection)))
            {
                accountData = accountData.OrderBy(dataTableParams.sortColumn + " " + dataTableParams.sortColumnDirection);
            }
            if (!string.IsNullOrEmpty(dataTableParams.SearchValue))
            {
                accountData = accountData.Where(m => m.Username.Contains(dataTableParams.SearchValue)
                                            || m.Password.Contains(dataTableParams.SearchValue));
            }
            recordsTotal = accountData.Count();
            var data = accountData.Skip(skip).Take(pageSize).ToList();


            return new GridViewModel<List<Account>>
            {
                Draw = dataTableParams.Draw,
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsTotal,
                Data = data
            };

        }

        public AjaxViewModel<string> Delete(int id)
        {
            var result = new AjaxViewModel<string>();
            var data = _context.Account.FirstOrDefault(x => x.ID == id);
            if (data != null)
            {
                _context.Account.Remove(data);
                _context.SaveChanges();
                result.IsSuccess = true;
                result.Message = "success";
                result.Data = null;
            }
            return result;

        }
    }
}
