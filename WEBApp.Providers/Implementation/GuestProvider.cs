﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.WEB.Models.Account;
using WEBApp.DataAccess.Contexts;
using WEBApp.DataAccess.Models;
using WEBApp.Providers.Interface;
using WEBApp.ViewModels;
using System.Linq.Dynamic.Core;

namespace WEBApp.Providers.Implementation
{
    public class GuestProvider : IGuestProvider
    {
        private readonly HotelContext _context;
        public GuestProvider(HotelContext context)
        {
            _context = context;
        }
        public void Create(CreateGuestViewModel model)
        {
            Tamu guest = new Tamu
            {
                NomorRegister = model.NomorRegister,
                NamaDepan = model.NamaDepan,
                NamaBelakang = model.NamaBelakang,
                TanggalLahir = model.TanggalLahir,
                TempatLahir = model.TempatLahir,
                JenisKelamin = model.JenisKelamin,
                NomorKTP = model.NomorKTP,
                CreatedDate = model.CreatedDate,
                CreatedBy = model.CreatedBy
            };
            _context.Tamu.Add(guest);
            _context.SaveChanges();

        }

        public GridViewModel<List<TamuViewModel>> GetAll(DataTablesParameters dataTableParams)
        {
            int pageSize = dataTableParams.Length != null ? Convert.ToInt32(dataTableParams.Length) : 0;
            int skip = dataTableParams.Start != null ? Convert.ToInt32(dataTableParams.Start) : 0;
            int recordsTotal = 0;
            var guestData = (from tempGuest in _context.Tamu
                             join tempJKelamin in _context.MstJenisKelamin on tempGuest.JenisKelamin equals tempJKelamin.ID
                             select new TamuViewModel
                             {
                                 ID = tempGuest.ID,
                                 JenisKelamin = tempJKelamin.Nama,
                                 NamaBelakang = tempGuest.NamaBelakang,
                                 NamaDepan = tempGuest.NamaDepan,
                                 NomorKTP = tempGuest.NomorKTP,
                                 NomorRegister = tempGuest.NomorRegister,
                                 TanggalLahir = tempGuest.TanggalLahir,
                                 TempatLahir = tempGuest.TempatLahir
                             });



            if (!(string.IsNullOrEmpty(dataTableParams.sortColumn) && string.IsNullOrEmpty(dataTableParams.sortColumnDirection)))
            {
                guestData = guestData.OrderBy(dataTableParams.sortColumn + " " + dataTableParams.sortColumnDirection);
            }
            if (!string.IsNullOrEmpty(dataTableParams.SearchValue))
            {
                guestData = guestData.Where(m => m.JenisKelamin.Contains(dataTableParams.SearchValue)
                                            || m.NamaBelakang.Contains(dataTableParams.SearchValue)
                                            || m.NamaDepan.Contains(dataTableParams.SearchValue)
                                            || m.NomorKTP.Contains(dataTableParams.SearchValue)
                                            || m.NomorRegister.Contains(dataTableParams.SearchValue)
                                            || m.TempatLahir.Contains(dataTableParams.SearchValue));
            }
            recordsTotal = guestData.Count();
            var data = guestData.Skip(skip).Take(pageSize).ToList();


            return new GridViewModel<List<TamuViewModel>>
            {
                Draw = dataTableParams.Draw,
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsTotal,
                Data = data
            };
        }
        public AjaxViewModel<string> Delete(int id)
        {
            var result = new AjaxViewModel<string>();
            var data = _context.Tamu.FirstOrDefault(x => x.ID == id);
            if (data != null)
            {
                _context.Tamu.Remove(data);
                _context.SaveChanges();
                result.IsSuccess = true;
                result.Message = "success";
                result.Data = null;
            }
            return result;

        }

        public AjaxViewModel<CreateGuestViewModel> GetDataById(int id)
        {
            var result = new AjaxViewModel<CreateGuestViewModel>();
            try
            {
                var data = (from guest in _context.Tamu
                            where guest.ID == id
                            select new CreateGuestViewModel()
                            {
                                Id = guest.ID,
                                NomorRegister = guest.NomorRegister,
                                NamaDepan = guest.NamaDepan,
                                NamaBelakang = guest.NamaBelakang,
                                TanggalLahir = guest.TanggalLahir,
                                TempatLahir = guest.TempatLahir,
                                JenisKelamin = guest.JenisKelamin,
                                NomorKTP = guest.NomorKTP
                            }).FirstOrDefault();
                if (data != null)
                {
                    result.IsSuccess = true;
                    result.Message = "success";
                    result.Data = data;
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "data not found";
                    result.Data = data;
                }
            }
            catch (Exception e)
            {

                result.IsSuccess = false;
                result.Message = e.Message;
            }
            
            return result;
        }

        public void Update(CreateGuestViewModel model, string ccurrentUser)
        {
            var data = _context.Tamu.FirstOrDefault(x => x.ID == model.Id);
            if (data != null)
            {
                data.NomorRegister = model.NomorRegister;
                data.NamaDepan = model.NamaDepan;
                data.NamaBelakang = model.NamaBelakang;
                data.TanggalLahir = model.TanggalLahir;
                data.TempatLahir = model.TempatLahir;
                data.JenisKelamin = model.JenisKelamin;
                data.NomorKTP = model.NomorKTP;
                data.UpdatedBy = ccurrentUser;
                data.UpdatedDate = DateTime.Now;
                _context.SaveChanges();
            }
            
        }
    }
}
