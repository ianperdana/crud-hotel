﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.WEB.Models.Account;
using WEBApp.DataAccess.Models;
using WEBApp.ViewModels;

namespace WEBApp.Providers.Interface
{
    public interface IAccountProvider
    {
        AccountViewModel Register(AccountViewModel model);
        LoginResponseViewModel Login(AccountViewModel model);
        string CreateToken(Account account);
        GridViewModel<List<Account>> GetAccount(DataTablesParameters dataTableParams);
        AjaxViewModel<string> Delete(int id);

    }
}
