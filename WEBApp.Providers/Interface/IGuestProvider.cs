﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEBApp.DataAccess.Models;
using WebApp.WEB.Models.Account;
using WEBApp.ViewModels;

namespace WEBApp.Providers.Interface
{
    public interface IGuestProvider
    {
        GridViewModel<List<TamuViewModel>> GetAll(DataTablesParameters dataTableParams);
        public void Create(CreateGuestViewModel model);
        AjaxViewModel<string> Delete(int id);
        AjaxViewModel<CreateGuestViewModel> GetDataById(int id);
        public void Update(CreateGuestViewModel model,string currentUser);



    }
}
