﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModels
{
    public class AccountViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
