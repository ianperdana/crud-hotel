﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModels
{
    public class CreateGuestViewModel
    {
        public int Id { get; set; }
        public string NomorRegister { get; set; } = null!;
        public string? NamaDepan { get; set; }
        public string? NamaBelakang { get; set; }
        public DateTime? TanggalLahir { get; set; }
        public string? TempatLahir { get; set; }
        public int? JenisKelamin { get; set; }
        public string? NomorKTP { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }

    }
}
