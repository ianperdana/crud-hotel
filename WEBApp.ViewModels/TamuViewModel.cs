﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEBApp.ViewModels
{
    public class TamuViewModel
    {
        public int ID { get; set; }
        public string NomorRegister { get; set; }
        public string NamaDepan { get; set; }
        public string NamaBelakang { get; set; }
        public DateTime? TanggalLahir { get; set; }
        public string TempatLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string NomorKTP { get; set; }

    }
}
