﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.WEB.Models.Account;
using WEBApp.DataAccess.Models;
using WEBApp.Providers.Interface;
using WEBApp.ViewModels;

namespace WEBApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountProvider _provider;
        public AccountController(IAccountProvider provider)
        {
            _provider = provider;
        }

        [HttpPost("[action]")]
        public IActionResult Register([FromBody] AccountViewModel model)
        {
            var ajaxViewModel = new AjaxViewModel<AccountViewModel>();
            try
            {
                var data = _provider.Register(model);
                ajaxViewModel.IsSuccess = true;
                ajaxViewModel.Message = "Register Success";
                ajaxViewModel.Data = data;
                return Ok(ajaxViewModel);
            }
            catch (Exception ex)
            {

                ajaxViewModel.IsSuccess = false;
                ajaxViewModel.Message = "Register Failed";
                return BadRequest(ajaxViewModel);
            }
            
            
        }
        [HttpPost("[action]")]
        public IActionResult Login([FromBody] AccountViewModel model)
        {
            var ajaxViewModel = new AjaxViewModel<LoginResponseViewModel>();
            try
            {
                var data = _provider.Login(model);
                if (data.Token != null)
                {
                    ajaxViewModel.IsSuccess = true;
                    ajaxViewModel.Message = "Success";
                    ajaxViewModel.Data = data;
                    return Ok(ajaxViewModel);
                }
                else
                {
                    ajaxViewModel.IsSuccess = false;
                    ajaxViewModel.Message = "Failed";
                    return BadRequest(ajaxViewModel);
                }
                
                

            }
            catch (Exception)
            {

                ajaxViewModel.IsSuccess = false;
                ajaxViewModel.Message = "Register Failed";
                return BadRequest(ajaxViewModel);
            }
           
            
        }

        [HttpPost("[action]")]
        [Authorize]
        public IActionResult GetAll([FromBody]DataTablesParameters dataTableParams)
        {
            var currentUSer = HttpContext.User.Identity.Name;
            var data = _provider.GetAccount(dataTableParams);
            return Ok(data);

        }
        [HttpPost("[action]")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            var data = _provider.Delete(id);
            return Ok(data);

        }

    }
}
