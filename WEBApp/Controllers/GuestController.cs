﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.WEB.Models.Account;
using WEBApp.Providers.Interface;
using WEBApp.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEBApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GuestController : ControllerBase
    {
        private readonly IGuestProvider _provider;

        public GuestController(IGuestProvider provider)
        {
            _provider = provider;
        }

        [HttpPost("[action]")]
        [Authorize]
        public IActionResult GetAll([FromBody] DataTablesParameters dataTableParams)
        {
            var currentUSer = HttpContext.User.Identity.Name;
            var data = _provider.GetAll(dataTableParams);
            return Ok(data);

        }

        // POST api/<GuestController>
        [HttpPost("[action]")]
        [Authorize]
        public IActionResult Create([FromBody] CreateGuestViewModel model)
        {
            var result = new AjaxViewModel<string>();
            
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                model.CreatedBy = currentUser;
                model.CreatedDate = DateTime.Now;
                _provider.Create(model);
                result.IsSuccess = true;
            }
            catch (Exception)
            {

                result.IsSuccess = false;
            }
            return Ok(result);
        }

        [HttpGet("[action]")]
        [Authorize]
        public IActionResult GetById(int id)
        {
            var data = _provider.GetDataById(id);
            return Ok(data);

        }

        [HttpPost("[action]")]
        [Authorize]
        public IActionResult Update([FromBody] CreateGuestViewModel value)
        {
            var result = new AjaxViewModel<string>();

            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                _provider.Update(value,currentUser);
                result.IsSuccess = true;
            }
            catch (Exception)
            {

                result.IsSuccess = false;
            }
            return Ok(result);

        }

        // DELETE api/<GuestController>/5

        [HttpPost("[action]")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            var data = _provider.Delete(id);
            return Ok(data);

        }
    }
}
