﻿using WEBApp.Providers.Implementation;
using WEBApp.Providers.Interface;

namespace WEBApp
{
    public static class DependencyInjection
    {
        public static void ConfigureAddScope(this IServiceCollection services)
        {
            services.AddTransient<IGuestProvider, GuestProvider>();
        }
    }
}
