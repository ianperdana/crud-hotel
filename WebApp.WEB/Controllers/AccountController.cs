﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using WEBApp.Helpers;
using WEBApp.ViewModels;

namespace WebApp.WEB.Controllers
{
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        public AccountController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SubmitRegister(RegisterViewModel collection)
        {
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Account/Register";
            var ajaxViewModel = new AjaxViewModel<AccountViewModel>();
            var model = new AccountViewModel
            {
                Username = collection.Username,
                Password = collection.Password
            };
            var json = JsonConvert.SerializeObject(model);
            try
            {
                ajaxViewModel = RestAPIHelper<AjaxViewModel<AccountViewModel>>.Submit(json, Method.POST, endpoint);
                if (ajaxViewModel.IsSuccess)
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    return RedirectToAction("Register");
                }

            }
            catch (Exception e)
            {
                return RedirectToAction("Register");
            }
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ValidateLogin([FromBody] AccountViewModel collection)
        {
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Account/Login";
            var ajaxViewModel = new AjaxViewModel<LoginResponseViewModel>();
            //var result = new AjaxViewModel<LoginResponseViewModel>();
            var model = new AccountViewModel
            {
                Username = collection.Username,
                Password = collection.Password
            };
            var json = JsonConvert.SerializeObject(model);
            try
            {
                ajaxViewModel = RestAPIHelper<AjaxViewModel<LoginResponseViewModel>>.Submit(json, Method.POST, endpoint, Request);
            }
            catch (Exception e)
            {

            }
            return Json(ajaxViewModel);
        }
    }
}
