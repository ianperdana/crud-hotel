﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using WebApp.WEB.Filter;
using WEBApp.DataAccess.Models;
using WEBApp.ViewModels;
using WebApp.WEB.Models.Account;
using WEBApp.Helpers;

namespace WebApp.WEB.Controllers
{
    public class GuestController : Controller
    {
        private readonly ILogger<GuestController> _logger;
        private readonly IConfiguration _configuration;
        public GuestController(ILogger<GuestController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [AuthorizationPrivilageFilter]
        public IActionResult Index()
        {
            return View();
        }

        [AuthorizationPrivilageFilter]
        [HttpPost]
        public IActionResult GetAllTamu(IFormCollection collection)
        {
            var data = new DataTablesParameters
            {
                Draw = Convert.ToInt32(collection["draw"].FirstOrDefault()),
                Start = Convert.ToInt32(collection["start"].FirstOrDefault()),
                Length = Convert.ToInt32(collection["length"].FirstOrDefault()),
                SearchValue = collection["search[value]"].FirstOrDefault(),
                sortColumn = collection["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(),
                sortColumnDirection = collection["order[0][dir]"].FirstOrDefault()
            };
            var json = JsonConvert.SerializeObject(data);
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Guest/GetAll";
            var jsonViewModel = new GridViewModel<List<TamuViewModel>>();
            var jsonData = new { draw = 0, recordsFiltered = 0, recordsTotal = 0, data = jsonViewModel.Data, isSuccess = false };
            try
            {
                jsonViewModel = RestAPIHelper<GridViewModel<List<TamuViewModel>>>.Submit(json, Method.POST, endpoint, Request);
                jsonData = new { draw = jsonViewModel.Draw, recordsFiltered = jsonViewModel.RecordsTotal, recordsTotal = jsonViewModel.RecordsTotal, data = jsonViewModel.Data, isSuccess = true };
            }
            catch (Exception e)
            {
                if (e.Message == "UnAuthorize")
                {
                    jsonData = new { draw = 0, recordsFiltered = 0, recordsTotal = 0, data = jsonViewModel.Data, isSuccess = false };
                }
            }
            return Json(jsonData);

        }
        [AuthorizationPrivilageFilter]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(CreateGuestViewModel collection)
        {
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Guest/Create";
            var ajaxViewModel = new AjaxViewModel<string>();
            var json = JsonConvert.SerializeObject(collection);
            try
            {
                ajaxViewModel = RestAPIHelper<AjaxViewModel<string>>.Submit(json, Method.POST, endpoint,Request);
                if (ajaxViewModel.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Create");
                }

            }
            catch (Exception e)
            {
                return RedirectToAction("Create");
            }
        }

        [AuthorizationPrivilageFilter]
        [HttpPost]
        public IActionResult DeleteTamu(int id)
        {
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Guest/Delete?id=" + id;
            var jsonViewModel = new AjaxViewModel<string>();
            var jsonData = new { isSuccess = false };
            try
            {
                jsonViewModel = RestAPIHelper<AjaxViewModel<string>>.Submit("", Method.POST, endpoint, Request);
                jsonData = new { isSuccess = true };
            }
            catch (Exception e)
            {
                if (e.Message == "UnAuthorize")
                {
                    jsonData = new { isSuccess = false };
                }
            }
            return Json(jsonData);

        }
        [AuthorizationPrivilageFilter]
        public IActionResult Edit(int id)
        {
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Guest/GetById?id=" + id;
            var jsonViewModel = new AjaxViewModel<CreateGuestViewModel>();
            try
            {
                jsonViewModel = RestAPIHelper<AjaxViewModel<CreateGuestViewModel>>.Submit("", Method.GET, endpoint, Request);
                
            }
            catch (Exception e)
            {
                if (e.Message == "UnAuthorize")
                {
                    
                    return RedirectToAction("Login","Account");
                }
            }
            return View(jsonViewModel.Data);

        }
        [HttpPost]
        public IActionResult Update(CreateGuestViewModel collection)
        {
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Guest/Update";
            var ajaxViewModel = new AjaxViewModel<string>();
            var json = JsonConvert.SerializeObject(collection);
            try
            {
                ajaxViewModel = RestAPIHelper<AjaxViewModel<string>>.Submit(json, Method.POST, endpoint, Request);
                if (ajaxViewModel.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Edit");
                }

            }
            catch (Exception e)
            {
                return RedirectToAction("Create");
            }
        }
    }
}
