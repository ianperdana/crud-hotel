﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Diagnostics;
using WebApp.WEB.Filter;
using WebApp.WEB.Models;
using WebApp.WEB.Models.Account;
using WEBApp.DataAccess.Models;
using WEBApp.Helpers;
using WEBApp.ViewModels;

namespace WebApp.WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        [AuthorizationPrivilageFilter]
        public IActionResult Index()
        {
            return View();
        }
        [AuthorizationPrivilageFilter]
        public IActionResult Privacy()
        {
            return View();
        }
        [AuthorizationPrivilageFilter]
        [HttpPost]
        public IActionResult GetAllAccount(IFormCollection collection)
        {
            var data = new DataTablesParameters
            {
                Draw = Convert.ToInt32(collection["draw"].FirstOrDefault()),
                Start = Convert.ToInt32(collection["start"].FirstOrDefault()),
                Length = Convert.ToInt32(collection["length"].FirstOrDefault()),
                SearchValue = collection["search[value]"].FirstOrDefault(),
                sortColumn = collection["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(),
                sortColumnDirection = collection["order[0][dir]"].FirstOrDefault()
            };
            var json = JsonConvert.SerializeObject(data);
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Account/GetAll";
            var jsonViewModel = new GridViewModel<List<Account>>();
            var jsonData = new { draw = 0, recordsFiltered = 0, recordsTotal = 0, data = jsonViewModel.Data, isSuccess = false };
            try
            {
                jsonViewModel = RestAPIHelper<GridViewModel<List<Account>>>.Submit(json, Method.POST, endpoint, Request);
                 jsonData = new { draw = jsonViewModel.Draw, recordsFiltered = jsonViewModel.RecordsTotal, recordsTotal = jsonViewModel.RecordsTotal, data = jsonViewModel.Data,isSuccess = true };
            }
            catch (Exception e)
            {
                if (e.Message == "UnAuthorize")
                {
                    jsonData = new { draw = 0, recordsFiltered = 0, recordsTotal = 0, data = jsonViewModel.Data, isSuccess = false };
                }
            }
            return Json(jsonData);

        }

        [AuthorizationPrivilageFilter]
        [HttpPost]
        public IActionResult DeleteAccount(int id)
        {
            var json = JsonConvert.SerializeObject(id);
            var endpoint = _configuration.GetSection("ApiUrl").Value + "/Account/Delete?id="+id;
            var jsonViewModel = new AjaxViewModel<string>();
            var jsonData = new { isSuccess = false };
            try
            {
                jsonViewModel = RestAPIHelper<AjaxViewModel<string>>.Submit("", Method.POST, endpoint, Request);
                jsonData = new { isSuccess = true };
            }
            catch (Exception e)
            {
                if (e.Message == "UnAuthorize")
                {
                    jsonData = new { isSuccess = false };
                }
            }
            return Json(jsonData);

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}