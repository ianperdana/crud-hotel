﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.WEB.Filter
{
    public class AuthorizationPrivilageFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var cookie = context.HttpContext.Request.Cookies["token"];
            if (cookie == null)
            {
                context.Result = new RedirectToRouteResult(new RouteValueDictionary{{ "controller", "Account" },
                                        { "action", "Login" }
                                        });
            }
            base.OnActionExecuting(context);

        }
    }
}
