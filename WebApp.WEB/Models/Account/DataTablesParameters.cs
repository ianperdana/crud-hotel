﻿namespace WebApp.WEB.Models.Account
{
    public class DataTablesParameters
    {
        public int Draw { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string SearchValue { get; set; } = string.Empty;
        public string sortColumn { get; set; }
        public string sortColumnDirection { get; set; }
    }
}
