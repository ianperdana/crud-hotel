﻿//$(document).ready(function () {
//    var dataTableParams = {
//        draw: 1, // Populate the 'draw' parameter as needed
//        start: 0, // Populate the 'start' parameter as needed
//        length: 10
//    };
//    $('#example').DataTable({
//        serverSide: true,
//        bProcessing: true,
//        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
//        ajax: {
//            url: 'https://localhost:7048/Home/GetAllAccount',
//            type: "POST",
//            data: JSON.stringify(dataTableParams),
//            dataSrc: 'data',
//            error: function (xhr, errorType, exception) {
//                // Handle the AJAX error and redirect to the login page
//                window.location.href = window.apiLoginUrl;
//            }
//        },
//        columns: [
//            { data: 'id' },
//            { data: 'username' },
//            { data: 'password' }
//        ],
//        initComplete: function (settings, json) {
//            var data = json.isSuccess;

//            if (data === false) {
//                // Handle the case when data is null
//                // You can redirect to the login page or display an error message.
//                window.location.href = window.apiLoginUrl;
//            } else {
//                // Your other code for processing and displaying the data
//            }
//        }
//    });
//});
$(document).ready(function () {
    $("#example").DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        "ajax": {
            "url": "https://localhost:7048/Home/GetAllAccount",
            "type": "POST",
            "datatype": "json"
        },
        //"columnDefs": [{
        //    "targets": [0],
        //    "visible": false,
        //    "searchable": false
        //}],
        "columns": [
            { "data": "id", "name": "Id", "autoWidth": true },
            { "data": "username", "name": "Username", "autoWidth": true },
            { "data": "password", "name": "Password", "autoWidth": true },
            {
                "render": function (data, type, row) {
                    return "<a href='#' class='btn btn-danger' onclick=\"DeleteAccount('" + row.id + "')\">Delete</a>";
                }
            },
        ]
    });
});
//const swalWithBootstrapButtons = Swal.mixin({
//    customClass: {
//        confirmButton: 'btn btn-success',
//        cancelButton: 'btn btn-danger'
//    },
//    buttonsStyling: false
//})

//swalWithBootstrapButtons.fire({
//    title: 'Are you sure?',
//    text: "You won't be able to revert this!",
//    icon: 'warning',
//    showCancelButton: true,
//    confirmButtonText: 'Yes, delete it!',
//    cancelButtonText: 'No, cancel!',
//    reverseButtons: true
//}).then((result) => {
//    if (result.isConfirmed) {
//        swalWithBootstrapButtons.fire(
//            'Deleted!',
//            'Your file has been deleted.',
//            'success'
//        )
//    } else if (
//        /* Read more about handling dismissals below */
//        result.dismiss === Swal.DismissReason.cancel
//    ) {
//        swalWithBootstrapButtons.fire(
//            'Cancelled',
//            'Your imaginary file is safe :)',
//            'error'
//        )
//    }
//})
function DeleteAccount(id) {
    Swal.fire({
        title: 'Do you want to Delete the changes?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Delete',
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                data: { id: id },
                // async: false,
                url: "https://localhost:7048/Home/DeleteAccount",
                success: function (result) {
                    if (result.isSuccess) {
                        swal.fire({
                            title: "Deleted!",
                            text: "success!",
                            type: "success"
                        }).then(function () {
                            window.location.reload();
                        });
                        
                    } else {
                        Swal.fire('Changes are not Deleted', '', 'error')
                    }

                },
                error: function (err) {
                    Swal.fire('Changes are not Deleted', '', 'error')
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Changes are not Deleted', '', 'info')
        }
    })

    

        //function (isConfirm) {
        //    if (isConfirm) {
        //        $.ajax({
        //            type: "POST",
        //            data: { id: id },
        //            // async: false,
        //            url: "https://localhost:7048/Home/DeleteAccount",
        //            success: function (result) {
        //                if (result.is_succeed) {
        //                    swal("Berhasil", "Data Berhasil Dihapus", "success");
        //                    location.reload();
        //                } else {
        //                    sweetAlert("Gagal", result.message, "error");
        //                }

        //            },
        //            error: function (err) {
        //                sweetAlert("Gagal", err.Message, "error");
        //            }
        //        });
        //    }
        //    else {
        //        swal("Dibatalkan", "", "error");
        //    }
        //});
}
