﻿$(document).ready(function () {
    $("#example").DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        "ajax": {
            "url": "https://localhost:7048/Guest/GetAllTamu",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs": [{
            "targets": [0],
            "visible": false,
            "searchable": false
        }],
        "columns": [
            { "data": "id", "name": "ID", "autoWidth": true },
            { "data": "nomorRegister", "name": "NomorRegister", "autoWidth": true },
            { "data": "namaDepan", "name": "NamaDepan", "autoWidth": true },
            { "data": "namaBelakang", "name": "NamaBelakang", "autoWidth": true },
            { "data": "tanggalLahir", "name": "TanggalLahir", "autoWidth": true },
            { "data": "tempatLahir", "name": "TempatLahir", "autoWidth": true },
            { "data": "jenisKelamin", "name": "JenisKelamin", "autoWidth": true },
            { "data": "nomorKTP", "name": "NomorKTP", "autoWidth": true },
            {
                "render": function (data, type, row) {
                    return "<a href='#' class='btn btn-danger' onclick=\"DeleteTamu('" + row.id + "')\">Delete</a>";
                }
            },
            {
                "render": function (data, type, row) {
                    return "<a href='#' class='btn btn-success' onclick=\"EditTamu('" + row.id + "')\">Edit</a>";
                }
            },
        ]
    });
});

function DeleteTamu(id) {
    Swal.fire({
        title: 'Do you want to Delete the changes?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Delete',
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                data: { id: id },
                // async: false,
                url: "https://localhost:7048/Guest/DeleteTamu",
                success: function (result) {
                    if (result.isSuccess) {
                        swal.fire({
                            title: "Deleted!",
                            text: "success!",
                            type: "success"
                        }).then(function () {
                            window.location.reload();
                        });

                    } else {
                        Swal.fire('Changes are not Deleted', '', 'error')
                    }

                },
                error: function (err) {
                    Swal.fire('Changes are not Deleted', '', 'error')
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Changes are not Deleted', '', 'info')
        }
    })
}

function EditTamu(id) {
    document.location = 'https://localhost:7048/Guest/Edit?id='+id
}
