﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var baseMvcUrl = 'https://localhost:7048';
var apiUrl = 'https://localhost:7206';


window.apiLoginUrl = `${baseMvcUrl}/Account/Login`;
window.mainMenuUrl = `${baseMvcUrl}/Home`;
window.loginUrl = `${baseMvcUrl}/Account/ValidateLogin`;
window.logoutUrl = `${baseMvcUrl}/Account/Logout`;
window.getTokenLoginUrl = `${baseMvcUrl}/User/GetTokenLogin`;




